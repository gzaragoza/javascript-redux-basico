import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore } from 'redux';

//Store -> un state globalizado


//------------------>

//Action -> son las funciones las que hacen las cosas - en este caso Incrementar
// es una función que return un objeto
const increment = () => {
  return {
    type: 'INCREMENT'
  }
}
const decrement = () => {
  return {
    type: 'DECREMENT'
  }
}

//------------------>

// Reducer -> describe como las acciones modifican 
//el state en el siguiente estado. Comprueba que hace las accion que has pasado y actua
// es el que hace el cambio en el state
const counter = (state = 0, action) => {
  switch(action.type) {
    case "INCREMENT":
      return state + 1;
    case "DECREMENT":
      return state -1;
  }

};

//------------------>

let store = createStore(counter);

//mostramos el store por la consola
store.subscribe(()=> console.log(store.getState()));


// Dispatch -> ejecuta la acción
// así es como llamamos a las actions // esta accion incrementa un valor
store.dispatch(increment());

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
